function isVow(a){
    return a.map(n => {
        let char = String.fromCharCode(n);
        return ['a', 'e', 'i', 'o', 'u'].includes(char) ? char : n;
    })
}