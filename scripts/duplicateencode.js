function duplicateEncode(word) {
    let m = new Map();
    word = word.toLowerCase();
    word.split("").forEach(ch => m.set(ch, m.get(ch) + 1 || 1));
    let res = '';
    for (let ch of word) {
        res += m.get(ch) === 1 ? '(' : ')';
    }
    return res;
}
