function comp(a, b){
    if (!a || !b) return false;
    for (let n of a) {
        let index = b.indexOf(n * n);
        if (index === -1) return false;
        b.splice(index, 1);
    }
    return true;
}