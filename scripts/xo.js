function XO(str) {
    let x = 0;
    let o = 0;

    for (let ch of str.toLowerCase()) {
        if (ch === 'x') {
            x++;
        } else if (ch === 'o') {
            o++;
        }
    }
    return x === o;
}