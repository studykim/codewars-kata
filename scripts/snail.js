snail = function(array) {
    let res = [];
    for (let step = 0; step < Math.floor(array.length / 2) ; step++) {
        for (let i = step; i < array.length - step - 1; i++) {
            res.push(array[step][i]);
        }
        for (let i = step; i < array.length - step - 1; i++) {
            res.push(array[i][array.length - step - 1]);
        }
        for (let i = array.length - step - 1; i > step; i--) {
            res.push(array[array.length - step - 1][i]);
        }
        for (let i = array.length - step - 1; i > step; i--) {
            res.push(array[i][step]);
        }
    }
    if (array[0].length > 0 && array.length % 2 === 1) {
        let mid = Math.floor(array.length / 2);
        res.push(array[mid][mid]);
    }
    return res;
}

console.log(snail([[]]))