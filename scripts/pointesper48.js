function pointsPer48(ppg, mpg) {
    return mpg ? Math.round(10 * ppg * 48 / mpg) / 10: 0;
}