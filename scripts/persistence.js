function persistence(num) {
    let res = 0
    while (num >= 10) {
        num = num.toString().split("").reduce((acc, cur) => acc * cur , 1);
        res++;
    }
    return res
}