function isIsogram(str){
    let s = new Set();
    for (let ch of str.toLowerCase()) {
        if (s.has(ch)) return false;
        s.add(ch);
    }
    return true;
}
