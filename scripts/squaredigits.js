function squareDigits(num) {
    return +String(num).split("").map(Number).map(n => n * n).join("");
}